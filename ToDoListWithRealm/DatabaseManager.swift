//
//  DatabaseManager.swift
//  ToDoListWithRealm
//
//  Created by Валерия П on 28.10.21.
//

import Foundation
import RealmSwift
import CoreData

class DatabaseManager {
    
    var realm = try! Realm()
    
    static let shared: DatabaseManager = DatabaseManager()
    private init() {}
    
    func getObjects()->[ToDoList] {
        let realm = try! Realm()
        let realmResults = realm.objects(ToDoList.self)
        return Array(realmResults)
        
    }
    
    func addToDo(titleToDo: String, descriptionToDo: String){
        let id: String = UUID().uuidString
        let newToDo: ToDoList = ToDoList(titleToDo: titleToDo, descriptionToDo: descriptionToDo,id: id)
        try! realm.write {
            realm.add(newToDo)
        }
    }
    
    func deleteObject(id: String) {
        let result = realm.objects(ToDoList.self).filter("id == %@", id)
        try! realm.write{
            for object in result{
                self.realm.delete(object)
            }
        }
    }
    
    func updateToDo(toDo: ToDoList, newTitleToDo: String,newdescriptionToDo: String) {
        try! realm.write{
        toDo.titleToDo = newTitleToDo
        toDo.descriptionToDo = newdescriptionToDo
        }
    }
    
    //    let path = realm.configuration.fileURL?.path
    //    print("Path\(String(describing: path))")
}
