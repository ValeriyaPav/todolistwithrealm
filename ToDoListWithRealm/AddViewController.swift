//
//  AddViewController.swift
//  ToDoListWithRealm
//
//  Created by Валерия П on 27.10.21.
//

import UIKit

protocol AddViewControllerDelegate: AnyObject {
    func appendToDo(titleToDo: String, descriptionToDo: String)
    func updateToDo(toDo: ToDoList, newTitleToDo: String, newdescriptionToDo: String)
}

enum StateToDo: Int, CaseIterable, Codable {
    case add
    case redact
}

class AddViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleTextField: UITextField!
    
    weak var delegate: AddViewControllerDelegate?
    
    var state: StateToDo = .add
    var indexPathCell: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleTextField.delegate = self
        self.descriptionTextView.delegate = self
        
        switch state {
        case .add:
            self.addButton.titleLabel?.text = "Save"
            self.navigationItem.title = "Add To Do"
        case .redact:
            self.addButton.titleLabel?.text = "Save changes"
            self.navigationItem.title = "Update To Do"
            let allToDo = DatabaseManager.shared.getObjects()
            if let indexPathCell = indexPathCell {
                let selectToDo: ToDoList = allToDo[indexPathCell]
                self.titleTextField.text = selectToDo.titleToDo
                self.descriptionTextView.text = selectToDo.descriptionToDo
            }
        }
    }
    
    
    @IBAction func addButtonAction(_ sender: Any) {
        switch state {
        case .add:
            addToDo()
        case .redact:
            redactToDo()
        }
    }
    
    func addToDo(){
        if let title = titleTextField.text{
            if title == ""{
                self.navigationController?.popViewController(animated: true)
            } else {
                self.delegate?.appendToDo(titleToDo: title, descriptionToDo: descriptionTextView.text ?? "")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func redactToDo(){
        let allToDo = DatabaseManager.shared.getObjects()
        if let indexPathCell = indexPathCell {
            let selectToDo: ToDoList = allToDo[indexPathCell]
            if let title = titleTextField.text, let description = descriptionTextView.text{
                if title != ""{
                    self.delegate?.updateToDo(toDo: selectToDo, newTitleToDo: title, newdescriptionToDo: description)
                }else{
                    
                }
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
}

