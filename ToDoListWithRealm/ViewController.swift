//
//  ViewController.swift
//  ToDoListWithRealm
//
//  Created by Валерия П on 27.10.21.
//
import Foundation
import UIKit
import RealmSwift

class ToDoList: Object {
    @objc dynamic var titleToDo = ""
    @objc dynamic var descriptionToDo = ""
    @objc dynamic var id = ""
    
    convenience init(titleToDo: String, descriptionToDo: String, id: String) {
        self.init()
        self.titleToDo = titleToDo
        self.descriptionToDo = descriptionToDo
        self.id = id
    }
}

class ViewController: UIViewController, AddViewControllerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var addButton: UIBarButtonItem!
    
    //    var allToDo = try! Realm().objects(ToDoList.self)
    var allToDo = DatabaseManager.shared.getObjects()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        self.addButton = UIBarButtonItem(title: nil, image: .add, primaryAction: UIAction(handler: { _ in
            self.pushAddViewController()
        }), menu: nil)
        
        self.navigationItem.rightBarButtonItem = addButton
        self.navigationItem.rightBarButtonItem?.tintColor = .black
    }
    
    
    func pushAddViewController() {
        if let addViewController: AddViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddViewController") as? AddViewController {
            addViewController.delegate = self
            self.navigationController?.pushViewController(addViewController, animated: true)
        }
    }
    
    func appendToDo(titleToDo: String, descriptionToDo: String) {
        DatabaseManager.shared.addToDo(titleToDo: titleToDo, descriptionToDo: descriptionToDo)
        self.allToDo = DatabaseManager.shared.getObjects()
        self.tableView.reloadData()
    }
    
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = allToDo[indexPath.row].titleToDo
        cell.detailTextLabel?.text = allToDo[indexPath.row].descriptionToDo
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allToDo.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let id: String = allToDo[indexPath.row].id
        DatabaseManager.shared.deleteObject(id: id)
        self.allToDo = DatabaseManager.shared.getObjects()
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let addViewController: AddViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddViewController") as? AddViewController {
            addViewController.state = .redact
            addViewController.indexPathCell = indexPath.row
            addViewController.delegate = self
            self.navigationController?.pushViewController(addViewController, animated: true)
        }
    }
    
    func updateToDo(toDo: ToDoList, newTitleToDo: String, newdescriptionToDo: String) {
        DatabaseManager.shared.updateToDo(toDo: toDo, newTitleToDo: newTitleToDo, newdescriptionToDo: newdescriptionToDo)
        self.allToDo = DatabaseManager.shared.getObjects()
        self.tableView.reloadData()
    }
}
